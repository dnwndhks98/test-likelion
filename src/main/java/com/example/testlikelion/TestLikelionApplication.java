package com.example.testlikelion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestLikelionApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestLikelionApplication.class, args);
    }

}
